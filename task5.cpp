﻿#include <iostream>
#include <cmath>

class Vector
{
private:
    double x = 0;
    double y = 0;
    double z = 0;

public:
    Vector()
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << "\n" << x << " " << y << " " << z << "\n";
    }
    void Module()
    {
        std::cout<<"\n" << "Module of vector: " << sqrt(x * x + y * y + z * z) << "\n";
    }
};

int main()
{
    Vector v(10, 10, 10);
    v.Show();
    v.Module();
}


